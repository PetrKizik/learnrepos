﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace ParserService
{
    public partial class Service1 : ServiceBase
    {
        S13Parser parser;

        public Service1()
        {
            InitializeComponent();

            this.CanStop = true;

            DataLayer.DataProvider.InitializeDB("Data Source = (LocalDb)\\MSSQLLocalDB; AttachDbFilename = C:\\Users\\User\\source\\repos\\GoodNewsAggregator\\GoodNewsAggregator\\App_Data\\Storage.mdf; Integrated Security = True");
        }

        protected override void OnStart(string[] args)
        {
            parser = new S13Parser();
            
            parser.timer = new Timer(new TimerCallback(parser.Tick), null, 2000, 1000);
        }

        protected override void OnStop()
        {
            parser.showMustGoOn = false;

            Thread.Sleep(1000);
        }

    }
}
