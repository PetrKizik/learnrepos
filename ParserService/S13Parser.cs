﻿using DataLayer;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParserService
{
    class S13Parser
    {
        public bool showMustGoOn = true;

        public Timer timer;

        int counter = 3600;

        public void Tick(object obj)
        {
            if (showMustGoOn)
            {
                counter++;
                if (counter >= 3600)
                {
                    counter = 0;
                    Parse();
                }
            }
            else timer.Dispose();
        }

        void Parse()
        {
            string[] LastNews = new HtmlWeb().Load("http://s13.ru").DocumentNode.Descendants("h3")
                .Select(x => x.ChildNodes[0].Attributes["href"].Value).ToArray();

            if (DataProvider.News.Count == 0)
                for (int i = LastNews.Length - 1; i >= 0; i--)
                {
                    DataProvider.News.Add(GetArticle(LastNews[i]));
                    
                }
            else
            {
                string url = DataProvider.News.GetLast().SourceURL;
                int firstID = Convert.ToInt32(url.Substring(url.LastIndexOf('/') + 1));
                int lastID = Convert.ToInt32(LastNews[0].Substring(LastNews[0].LastIndexOf('/') + 1));

                firstID++;

                for (int i = firstID + 1; i <= lastID; i++)
                    DataProvider.News.Add(GetArticle("http://s13.ru/archives/" + i.ToString()));
            }
        }

        Article GetArticle(string URL)
        {
            HtmlNode[] articleHTML = new HtmlWeb().Load(URL).DocumentNode.Descendants()
                .Where(x => x.Attributes.Contains("class") && x.Attributes["class"].Value == "item entry").ToArray();

            if (articleHTML.Length == 0)
                return null;

            return new Article()
            {
                SourceURL = URL,

                Title = articleHTML[0].Descendants().First(x => x.Attributes.Contains("rel") && x.Attributes["rel"].Value == "bookmark").InnerText,

                Body = String.Concat(articleHTML[0].Descendants()
                .First(x => x.Attributes.Contains("class") && x.Attributes["class"].Value == "itemtext js-mediator-article")
                .Descendants("p").Select(x => x.OuterHtml)),

                PublishTime = DateTime.ParseExact(articleHTML[0].Descendants()
                .First(x => x.Attributes.Contains("class") && x.Attributes["class"].Value == "metadata")
                .InnerHtml.Split(new string[] { "</strong>.", "<br>" }, 0)[1].Trim(),
                "d MMMM yyyy 'года,' HH:mm",
                CultureInfo.GetCultureInfoByIetfLanguageTag("ru-RU"))
            };
        }
    }
}
