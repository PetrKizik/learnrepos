﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class NewsStorage
    {
        DataContext DB;

        public int Count { get { return DB.Articles.Count(); } }

        List<int> _idList;
        public int[] IdList
        {
            get
            {
                if (Count == 0)
                    return null;

                if (_idList == null || _idList.Count != Count)
                {
                    _idList = DB.Articles.OrderBy(x => x.PublishTime).Select(a => a.ID).ToList();
                    _idList.Reverse();
                }

                return _idList.ToArray();
            }
        }

        NewsStorage() { }

        internal NewsStorage(DataContext dataContext)
        {
            DB = dataContext;
        }

        public void Add(Article article)
        {
            if (article != null && DB.Articles.Where(x => x.SourceURL == article.SourceURL).ToArray().Count() == 0)
            {
                DB.Articles.Add(article);
                DB.SaveChanges();
            }
        }

        public Article Get (int id)
        {
            if (DB.Articles.Count() > 0)
                return DB.Articles.FirstOrDefault(i => i.ID == id);
            else return null;
        }

        public Article GetLast ()
        {
            if (DB.Articles.Count() > 0)
                return DB.Articles.First(i => i.PublishTime == DB.Articles.Max(t => t.PublishTime));
            else return null;
        }

        public List<Article> GetAll()
        {
            List<Article> artList = DB.Articles.OrderBy(x => x.PublishTime).ToList();
            artList.Reverse();
            return artList;
        }
    }
}
