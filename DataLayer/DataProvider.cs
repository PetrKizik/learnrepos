﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DataLayer
{
    public static class DataProvider
    {
        static NewsStorage _news;
        public static NewsStorage News { get { return _news; } }

        public static void InitializeDB(string connection_string)
        {
            _news = new NewsStorage(new DataContext(connection_string));
        }
    }

    class DataContext : DbContext
    {
        public DataContext(string connection_string) : base(connection_string) { }
        public DbSet<Article> Articles { get; set; }
    }

    public class Article
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string SourceURL { get; set; }
        public DateTime PublishTime { get; set; }
    }
}
