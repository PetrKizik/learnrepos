﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GoodNewsAggregator.Startup))]
namespace GoodNewsAggregator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
