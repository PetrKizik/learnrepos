﻿using DataLayer;
using GoodNewsAggregator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;

namespace GoodNewsAggregator
{
    public static class Paginator
    {
        public static ArticleHeadersViewModel GetArticleHeaders(int page, int itemsOnPage, out MvcHtmlString paginator)
        {
            int[] IdList = DataProvider.News.IdList;

            int maxPage = IdList.Length / itemsOnPage;
            if (IdList.Length % itemsOnPage != 0)
                maxPage++;

            if (page < 1) page = 1;
            if (page > maxPage) page = maxPage;

            paginator = GetHtmlCode(page, maxPage, 7);

            
            List<ArticleHeader> Headers = new List<ArticleHeader>();

            if (IdList != null && ((page - 1) * itemsOnPage < IdList.Length))
            {
                int maxIdIndex = page * itemsOnPage - 1;
                if (maxIdIndex >= IdList.Length)
                    maxIdIndex = IdList.Length - 1;

                for (int idIndex = (page - 1) * itemsOnPage; idIndex <= maxIdIndex; idIndex++)
                    Headers.Add(new ArticleHeader { ID = IdList[idIndex], Header = DataProvider.News.Get(IdList[idIndex]).Title });
            }

            return new ArticleHeadersViewModel { ArticleHeaders = Headers };
        }

        public static MvcHtmlString GetHtmlCode(int currentPage, int maxPage, int buttonsCount)
        {
            int MaxRange = maxPage / buttonsCount; //null-based
            if (maxPage % buttonsCount == 0)
                MaxRange--;

            int CurrentRange = currentPage / buttonsCount;
            if (currentPage % buttonsCount == 0)
                CurrentRange--;

            string Code = "<p>Page " + currentPage + " of " + maxPage + "</p>";
                Code += "<nav aria-label=\"...\"><ul class=\"pagination justify-content-center\">";

            if (CurrentRange == 0)
                Code += "<li class=\"page-item disabled\"><span class=\"page-link\">«</span></li>";
            else
                Code += "<li class=\"page-item\"><a class=\"page-link\" href=\"/Home/Index/?page="
                    + (currentPage - buttonsCount)
                    + "\">«</a></li>";

            for (int i = CurrentRange * buttonsCount + 1; i <= (CurrentRange + 1) * buttonsCount; i++)
                if (i == currentPage)
                    Code += "<li class=\"page-item active\"><span class=\"page-link\">" + i + "<span class=\"sr-only\">(current)</span></span></li>";
                else if (i <= maxPage)
                    Code += "<li class=\"page-item\"><a class=\"page-link\" href=\"/Home/Index/?page=" + i + "\">" + i + "</a></li>";

            if (CurrentRange == MaxRange)
                Code += "<li class=\"page-item disabled\"><span class=\"page-link\">»</span></li>";
            else
                Code += "<li class=\"page-item\"><a class=\"page-link\" href=\"/Home/Index/?page="
                    + (currentPage + buttonsCount > maxPage ? maxPage : currentPage + buttonsCount)
                    + "\">»</a></li>";

            Code += "</ul></nav>";

            return MvcHtmlString.Create(Code);
        }
    }
}