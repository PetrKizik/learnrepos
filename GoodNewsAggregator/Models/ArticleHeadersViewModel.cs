﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodNewsAggregator.Models
{
    public class ArticleHeadersViewModel
    {
        public List<ArticleHeader> ArticleHeaders { get; set; }
    }

    public class ArticleHeader
    {
        public int ID { get; set; }
        public string Header { get; set; }
    }
}