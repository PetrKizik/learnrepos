﻿using DataLayer;
using GoodNewsAggregator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodNewsAggregator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int page = 1)
        {
            int HeadersOnPage = 30;

            MvcHtmlString paginatorCode;

            ArticleHeadersViewModel viewModel = Paginator.GetArticleHeaders(page, HeadersOnPage, out paginatorCode);

            ViewBag.Paginator = paginatorCode;

            return View(viewModel);
        }

        public ActionResult ReadArticle(int? id)
        {
            if(id==null)
                return Redirect("/");
            Article art = DataProvider.News.Get((int)id);

            if (art == null)
                return Redirect("/");
            else
                return View(art);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}